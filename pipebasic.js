var fs = require('fs');

// creates a read stream
var readable = fs.createReadStream(__dirname + '/loreum.txt');

// creates a write stream
var writable = fs.createWriteStream(__dirname + '/ipsum.txt');

// pipe the readable stream object to writable stream
readable.pipe(writable);