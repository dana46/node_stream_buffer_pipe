var fs = require('fs'); // nodejs fs module

/**
 * fs.createReadStream to create a readable stream of buffer
 * @params
 * filePath : path to the file to be read
 * { encoding : 'utf8' } => to specify the character encoding :: defaults to 'utf8'
 * { highWaterMark : BYTES } => no of bytes to be read in a buffer at a time :: 16KB buffer size 
 */
var readable = fs.createReadStream(__dirname + '/loreum.txt', {encoding : 'utf8', highWaterMark : 16 * 1024});

// creates a writable stream
var writable = fs.createWriteStream(__dirname + '/ipsum.txt');

/**
 * ReadStream extends the EventEmitter, hence it has events such as on('data') read, do something i.e. (callback)
 */
readable.on('data', function(chunk) {
  console.log(chunk.length); // change the size of the highWaterMark to see different Buffer Size read
  writable.write(chunk);  // WriteStream writes the chunk everytime a buffer is read
});


/**
 * Creating a read stream & then writing it to a write stream is so common, that there is concept of PIPE.
 */