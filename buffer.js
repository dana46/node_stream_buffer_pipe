/**
 * Buffer : Temporary spot for holding data to move it from one place to another [server to browser]
 * Generally, data is stored in the Character Encoded format ['utf8' for example]
 */
var buff = new Buffer('Hello World!', 'utf8');

// displays the Code Point values in the buffer
console.log(buff);
// displays the string representation of the buffer
console.log(buff.toString());
// displays the length of the buffer
console.log(buff.length);
// displays the JSON representation the buffer
console.log(buff.toJSON());


// updated Buffer usage
var buff1 = Buffer.from('Hello World Again!');
console.log(buff1);
console.log(buff1.toString());
buff1.write('Yello'); // replaces the existing buffer => default starts from 0 index ; try buff1.write('Yello', 2)
console.log(buff1.toString());

/**
 * Read more about how data is stored in computer ! Binary Data, Character Sets, Character Encoding!
 * 
 * External Resource : 
 * https://medium.freecodecamp.org/do-you-want-a-better-understanding-of-buffer-in-node-js-check-this-out-2e29de2968e8
 */
