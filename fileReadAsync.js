var fs = require('fs'); // nodejs fs module

/**
 * Synchronous method to read from File :: BLOCKING
 */
console.log('-------------------- SYNC READ START! --------------------');
var readSync = fs.readFileSync(__dirname + '/hello.txt', 'utf8');
console.log(readSync);

console.log('-------------------- SYNC READ DONE! --------------------');

/**
 * Asynchronous method to read from File :: NON-BLOCKING
 */

var readAsync = fs.readFile(__dirname + '/hello.txt', 'utf8', function(err, data) {
  if(err) 
    console.log(err);
  console.log('------------------- ASYNC READ START! --------------------');
  console.log(data);
  console.log('------------------- ASYNC READ DONE! --------------------');
});

console.log('DONE! :: Node executes me ahead of the completion of Async File Read!');


/**
 * Check the sequence in which the console log happens!
 * fs.readFileSync => Synchronous method to read file
 * fs.readFile     => Asynchronous method to read file
 */

  /**
  * But, the problem with the above code is that file is read and stored in Buffer i.e. in-memnory
  * Hence, if many requests comes to read the file, then the in-memory might get BLOWN UP!!
  * How to solve???? Let's start thinking toward STREAMS !!!!
  */

