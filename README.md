# Buffer/Stream/Pipe

## Buffer

> _Node.js provides Buffer class which provide instances to store data in raw form, primarily encoded data._
> A temporary holding spot for data being moved from one place to another.

Create a new buffer
```javascript
var buff = new Buffer('Hello World!', 'utf8')
```
Updated buffer usage
```javascript
var buff1 = Buffer.from('Hello World Again!');
console.log(buff1);
console.log(buff1.toString());
buff1.write('Yello'); // replaces the existing buffer => default starts from 0 index
console.log(buff1.toString());
```

*File* : buffer.js

A good article on [Binary Data, Character Sets, Character Encoding](https://medium.freecodecamp.org/do-you-want-a-better-understanding-of-buffer-in-node-js-check-this-out-2e29de2968e8)

## File Read

Checkout the program for simple synchronous and asynchronous way to read a file.

```javascript
var fs = require('fs'); // file system module
```
Synchronous way
```javascript
var readFileSync = fs.readFileSync(__dirname + '/hello.txt', 'utf8');
```
Asynchronous way
```javascript
var readAsync = fs.readFile(__dirname + '/hello.txt', 'utf8', callbackFunction(err, data) { ... });
```
*File* : fileReadAsync.js

## Stream

> Streams are objects that lets you read data from a source or write data to the destination in a continuous fashion.
> A sequence of data made available over time.
> Pieces of data that eventually combine to the whole.

Reading a file and writing to another.
```javascript
var fs = require('fs');

// creates a readable stream
var readable = fs.createReadStream(__dirname + '/loreum.txt', {encoding : 'utf8', highWaterMark : 16 * 1024});

// creates a writable stream
var writable = fs.createWriteStream(__dirname + '/ipsum.txt');

readable.on('data', function(chunk) {
  console.log(chunk.length); // change the size of the highWaterMark to see different Buffer Size read
  writable.write(chunk);  // WriteStream writes the chunk everytime a buffer is read
});
```

*File* : fileStream.js

## Pipe

> Creating a read stream & then writing it to a write stream is so common, that there is concept of pipe.
> Piping simply means reading from a readable stream and writing it to a writable stream.

```javascript
var fs = require('fs');

// creates a read stream
var readable = fs.createReadStream(__dirname + '/loreum.txt');

// creates a write stream
var writable = fs.createWriteStream(__dirname + '/ipsum.txt');

// pipe the readable stream object to writable stream
readable.pipe(writable);
```

*File* : pipebasic.js

## Using Zlib for reading a file and creating a Zip file

```javascript
var fs = require('fs');
var zlib = require('zlib'); // nodejs zlib module

var readable = fs.createReadStream(__dirname + '/loreum.txt');

var compressed = fs.createWriteStream(__dirname + '/compressed.txt.gz');

// creating a duplex kind of readable/writable Stream
var gzip = zlib.createGzip();

readable.pipe(gzip).pipe(compressed);
```
*File* : pipeZlib.js

