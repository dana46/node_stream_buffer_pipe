var fs = require('fs');
var zlib = require('zlib'); // nodejs zlib module

var readable = fs.createReadStream(__dirname + '/loreum.txt');

var compressed = fs.createWriteStream(__dirname + '/compressed.txt.gz');

// creating a duplex kind of readable/writable Stream
var gzip = zlib.createGzip();

/**
 * piping readable stream to write into writable compressed gzip stream
 * piping readable gzip to writable compressed compressed.txt.gz
 */
readable.pipe(gzip).pipe(compressed);